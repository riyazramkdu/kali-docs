---
title: NetHunter VNC Manager
description:
icon:
date: 2019-10-26
type: post
weight: 100
author: ["g0tmi1k",]
tags: ["",]
keywords: ["",]
og_description:
---

The VNC service behaves differently from other system services so you can manage it via this pane in the NetHunter application. By default, the VNC service will only listen on the _localhost_ interface but you can de-select the checkbox to have it listen externally if you so desire.
